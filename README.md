# ricoeur-tei-utils #

Provides the Racket collection `ricoeur/tei`,
which contains utilities for working with TEI XML files
(tailored for Digital Ricoeur).

Some of these tools use the utility `xmllint` from libxml2 if it is installed.

