#lang racket/gui

(require framework
         ricoeur/tei/base
         ricoeur/tei/tools/tei-lint/lib
         "interfaces.rkt"
         "status-dot.rkt"
         "menu.rkt"
         "proto-frame.rkt"
         (submod "proto-frame.rkt" private)
         )

(provide (contract-out
          [error-proto-frame%
           (class/c
            (init [path path-string?]
                  [dir-frame dir-frame/false/c]
                  [val (or/c exn:fail? string?)]))]
          ))

(define error-proto-frame%
  (class proto-frame%
    (init path val)
    (inherit get-dir-frame/false)
    (super-new
     [lint-status 'error]
     [maybe-title (nothing)]
     [make-frame
      (λ ()
        (new error-frame%
             [path path]
             [dir-frame (get-dir-frame/false)]
             [val val]))])))

          
(define error-frame%
  (class dir-menu-bar-frame%
    (init path val)
    (super-new [label (gui-utils:quote-literal-label
                       #:quote-amp? #f
                       (string-append (path->string* path)
                                      " - TEI Lint"))]
               [alignment '(center top)]
               [width 400]
               [height 500])
    (let ([row (new horizontal-pane%
                    [parent this]
                    [alignment '(left center)])])
      (new status-canvas%
           [status 'error]
           [parent row])
      (new path-message%
           [parent row]
           [font bold-system-font]
           [path path]))
    (new message%
         [parent this]
         [label (if (string? val)
                    "xmllint found an error."
                    "The file is invalid.")])
    (new constant-editor-canvas%
         [parent this]
         [min-height 400]
         [allow-tab-exit #t]
         [content (match val
                    [(exn:fail msg _) msg]
                    [str str])])
    #|END class error-frame%|#))

